import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';
import { AppState } from '../../redux/reducers/index';
import { ConfigActions } from '../../redux/actions/config.actions';
import { FirebaseLoginService } from '../../services/firebaseLogin.service';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

    user: firebase.User;
    userLoginSubscriber;

    constructor(
        private _store: Store<AppState>,
        private configActions: ConfigActions,
        public flashMessage: FlashMessagesService,
        private router: Router,
        private firebaseLoginService: FirebaseLoginService
      ) { }
    ngOnInit() { }

    login() {
        // this._store.dispatch(this.configActions.login());
        this.firebaseLoginService.login();
    }

    logout() {

        this.firebaseLoginService.logout();
        // this._store.dispatch(this.configActions.logout());
        // this.flashMessage.show('You are logged out', {cssClass: 'alert-success', timeout: 3000});
        // this.router.navigate(['/']);
    }
}
