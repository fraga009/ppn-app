import { Component, OnInit } from '@angular/core';
import { IRace, IRaceType } from '../../../interfaces/race.interface';
import { Store } from '@ngrx/store';
import { AppState } from '../../../redux/reducers/index';
import { RaceActions } from '../../../redux/actions/race.actions';

@Component({
    selector: 'app-add-race',
    templateUrl: './add-race.component.html',
    styleUrls: ['./add-race.component.css']
})
export class AddRaceComponent implements OnInit {
    title: string;
    description: string;
    date: Date;
    deadline: Date;
    avaibleFrom: Date;

    defaultRaceType: IRaceType = {
        name: 'default',
        categories: []
    };

    constructor(
        private _store: Store<AppState>,
        private _raceActions: RaceActions
    ) { }

    ngOnInit() {
    }

    onAddSubmit() {
        console.log('onAddSubmit()');
        const race: IRace = {
            title: this.title,
            description: this.description,
            raceType: this.defaultRaceType,
            date: this.date,
            deadline: this.deadline,
            avaibleFrom: this.avaibleFrom
        };

        console.log(race);

        this._store.dispatch(this._raceActions.addRace(race));
    }
}
