import { Component, OnInit } from '@angular/core';
import { ICategory } from '../../../interfaces/category.interface';
import { Store } from '@ngrx/store';
import { AppState } from '../../../redux/reducers/index';
import { CategoryActions } from '../../../redux/actions/category.actions';
import { ISex } from '../../../interfaces/configs.interface';

@Component({
    selector: 'app-add-category',
    templateUrl: './add-category.component.html',
    styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {

    name: string;
    sex: any[];
    selectedSex: ISex;
    sexInterface = ISex;
    ageStart: Date;
    ageEnd: Date;
    noAgeLimits: boolean;

    sexArray(): Array<string> {
        const keys = Object.keys(this.sexInterface);
        return keys.slice(keys.length / 2);
    }

    constructor(
        private _store: Store<AppState>,
        private _categoryActions: CategoryActions
    ) {
        // this.sex = Object.keys(this.sexInterface);
        // this.sex.slice(this.sex.length / 2, this.sex.length - 1);
        // this.selectedSex = ISex.female;
    }

    ngOnInit() {
    }

    onAddSubmit() {
        console.log('onAddSubmit()');
        const category: ICategory = {
            name: this.name,
            sex: this.selectedSex,
            ageStart: this.ageStart,
            ageEnd: this.ageEnd,
            noAgeLimits: this.noAgeLimits
        };

        console.log(category);

        this._store.dispatch(this._categoryActions.addCategory(category));
    }
}
