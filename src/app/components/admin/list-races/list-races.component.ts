import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState, RaceState } from '../../../redux/reducers/index';
import { Subscription } from 'rxjs/Subscription';
import { RaceActions } from '../../../redux/actions/race.actions';
import { IRace } from '../../../interfaces/race.interface';

@Component({
    selector: 'app-list-races',
    templateUrl: './list-races.component.html',
    styleUrls: ['./list-races.component.css']
})
export class ListRacesComponent implements OnInit {

    raceReduxSubscriber: Subscription;
    currentState: RaceState;

    races: IRace[];

    constructor(
        private _store: Store<AppState>,
        private raceActions: RaceActions
    ) { }

    ngOnInit() {

        this.raceReduxSubscriber = this._store.select('race').subscribe((state: RaceState) => {
            this.currentState = state;

            console.log('°°°°° UPDATING RaceState:', state);
            this.races = state.races;
        });

    }

}
