import { Component } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { FirebaseService } from '../../services/firebase.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  superheroes: any;

  constructor(
    public flashMessage: FlashMessagesService,
    private firebaseService: FirebaseService
  ) { }


  getSuperheroes() {
    this.firebaseService.getsuperheroes().subscribe(superheroes => {
      const storageRef = firebase.storage().ref();
      superheroes.forEach(function(item, index) {
        storageRef.child(item.path).getDownloadURL().then((url) => {
          item.imageUrl = url;
        }).catch((error) => {
          console.log(error);
        });
      });
      this.superheroes = superheroes;
    });
  }
}
