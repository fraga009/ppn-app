import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { IRace } from '../interfaces/race.interface';
import { Store } from '@ngrx/store';
import { AppState } from '../redux/reducers/index';
import { RaceActions } from '../redux/actions/race.actions';

@Injectable()
export class RaceService {
    racesObservable: FirebaseListObservable<IRace[]>;
    race: FirebaseObjectObservable<IRace>;

    races: IRace[] = [];

    constructor(
        private af: AngularFireDatabase,
        private _store: Store<AppState>,
        private raceActions: RaceActions
    ) {
        this.racesObservable = this.af.list('/races') as FirebaseListObservable<IRace[]>;

        this.racesObservable.subscribe(races => {
            console.log('this.racesObservable.subscribe', races);
            this.races = races;

            this._store.dispatch(this.raceActions.getRaces());
        });
    }

    getRaces() {
        console.log('$$$$$$ getRaces()');
        return this.races;
    }

    getRaceDetails(id) {
        this.race = this.af.object('/races/' + id) as FirebaseObjectObservable<IRace>;
        return this.race;
    }

    addRace(race: IRace) {
        console.log('$$$$$$ addRace(race: IRace)');
        console.log(race);
        return this.racesObservable.push(race);
    }

    updateRace(id, race: IRace) {
        return this.racesObservable.update(id, race);
    }

    deleteRace(id) {
        return this.racesObservable.remove(id);
    }

}
