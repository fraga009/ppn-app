import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { Store } from '@ngrx/store';
import { AppState } from '../redux/reducers/index';
import { ICategory } from '../interfaces/category.interface';
import { CategoryActions } from '../redux/actions/category.actions';

@Injectable()
export class CategoryService {
    categoriesObservable: FirebaseListObservable<ICategory[]>;
    category: FirebaseObjectObservable<ICategory>;

    categories: ICategory[] = [];

    constructor(
        private af: AngularFireDatabase,
        private _store: Store<AppState>,
        private categoryActions: CategoryActions
    ) {
        this.categoriesObservable = this.af.list('/settings/categories') as FirebaseListObservable<ICategory[]>;

        this.categoriesObservable.subscribe(categories => {
            console.log('this.categoriesObservable.subscribe', categories);
            this.categories = categories;

            this._store.dispatch(this.categoryActions.getCategories());
        });
    }

    getCategories() {
        console.log('$$$$$$ getCategories()');
        return this.categories;
    }

    getCategoryDetails(id) {
        this.category = this.af.object('/settings/categories/' + id) as FirebaseObjectObservable<ICategory>;
        return this.category;
    }

    addCategory(category: ICategory) {
        console.log('$$$$$$ addCategory(category: ICategory)');
        console.log(category);
        return this.categoriesObservable.push(category);
    }

    updateCategory(id, category: ICategory) {
        return this.categoriesObservable.update(id, category);
    }

    deleteCategory(id) {
        return this.categoriesObservable.remove(id);
    }

}
