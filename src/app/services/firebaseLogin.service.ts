import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';

import { Store } from '@ngrx/store';
import { ConfigActions } from '../redux/actions/config.actions';
import { AppState } from '../redux/reducers/index';

@Injectable()
export class FirebaseLoginService {

    constructor(
        private afAuth: AngularFireAuth,
        private _store: Store<AppState>,
        private configActions: ConfigActions
    ) { }

    login() {
        this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(
            user => {
                this._store.dispatch(this.configActions.loginSuccess(user));
            }
        );
    }
    logout() {
        this.afAuth.auth.signOut().then(
            user => {
                this._store.dispatch(this.configActions.logoutSuccess());
            }
        );
    }

}