import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';

@Injectable()
export class FirebaseService {
  superheroes: FirebaseListObservable<any[]>;
  superhero: FirebaseObjectObservable<any>;
  folder: any;

  constructor(private af: AngularFireDatabase) {
    this.folder = 'superheroimages';
    this.superheroes = this.af.list('/superheroes') as FirebaseListObservable<superhero[]>;
  }

  getsuperheroes() {
    return this.superheroes;
  }

  getsuperheroDetails(id) {
    this.superhero = this.af.object('/superheroes/' + id) as FirebaseObjectObservable<superhero>;
    return this.superhero;
  }

  addsuperhero(superhero) {

    const storageRef = firebase.storage().ref();

    for (const selectedFile of [(<HTMLInputElement>document.getElementById('image')).files[0]]){
      const path = `/${this.folder}/${selectedFile.name}`;
      const iRef = storageRef.child(path);
      iRef.put(selectedFile).then((snapshot) => {
        superhero.image = selectedFile.name;
        superhero.path = path;
        superhero.pathThumb = `/${this.folder}/thumb_${selectedFile.name}`;
        return this.superheroes.push(superhero);
      });
    }
  }

  updatesuperhero(id, superhero) {
    return this.superheroes.update(id, superhero);
  }

  deletesuperhero(id) {
    return this.superheroes.remove(id);
  }

}

interface superhero {
  $key?: string;
  name?: string;
  city?: string;
  superpowers?: string;
  image?: string;
}