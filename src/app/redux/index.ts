import { configReducer } from './reducers/config.reducer';
import { raceReducer } from './reducers/race.reducer';

export const APP_REDUCERS = {
    config: configReducer,
    race: raceReducer
};
