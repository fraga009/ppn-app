import { ActionReducer, Action } from '@ngrx/store';
import { ConfigState } from './index';
import { ConfigActionsCreator } from '../actions/config.actions';

export const intitialConfigState: ConfigState = {
  loading: false,
  loggedIn: false
};

export const configReducer: ActionReducer<ConfigState> = (state = intitialConfigState, action: Action) => {

    switch (action.type) {

        case ConfigActionsCreator.LOGIN_SUCCESS: {
            return {
                ...state,
                loggedIn: true
            };
        }

        case ConfigActionsCreator.LOGOUT_SUCCESS: {
            return {
                ...state,
                loggedIn: false
            };
        }

        default: {
            return state;
        }
    }
};
