import { ActionReducer, Action } from '@ngrx/store';
import { RaceState } from './index';
import { RaceActionsCreator } from '../actions/race.actions';

export const intitialRaceState: RaceState = {
  races: null
};

export const raceReducer: ActionReducer<RaceState> = (state = intitialRaceState, action: Action) => {

    switch (action.type) {

        case RaceActionsCreator.RACE_LOADED: {
            console.log('case RaceActionsCreator.RACE_LOADED:', action.payload.races);
            return {
                races: action.payload.races
            };
        }

        default: {
            return state;
        }
    }
};
