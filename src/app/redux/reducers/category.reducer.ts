import { ActionReducer, Action } from '@ngrx/store';
import { CategoryState } from './index';
import { CategoryActionsCreator } from '../actions/category.actions';

export const intitialCategoryState: CategoryState = {
  categories: null
};

export const categoryReducer: ActionReducer<CategoryState> = (state = intitialCategoryState, action: Action) => {

    switch (action.type) {

        case CategoryActionsCreator.CATEGORY_LOADED: {
            console.log('case CategoryActionsCreator.CATEGORY_LOADED:', action.payload.category);
            return {
                categories: action.payload.category
            };
        }

        default: {
            return state;
        }
    }
};
