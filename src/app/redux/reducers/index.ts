// Interfaces
import { IRace } from '../../interfaces/race.interface';
import { IMember } from '../../interfaces/member.interface';
import { ICategory } from '../../interfaces/category.interface';

// Reducers
export * from './config.reducer';

export interface ConfigState {
    loading: boolean;
    loggedIn: boolean;
}
export interface RaceState {
    races: IRace[];
}
export interface CategoryState {
    categories: ICategory[];
}
export interface MemberState {
    members: IMember[];
}

/**
 * We treat each reducer like a table in a database.
 * This means our top level state interface is just a map of keys to inner state types.
 */
export interface AppState {
    config: ConfigState;
    race: RaceState;
    category: CategoryState;
    member: MemberState;
}
