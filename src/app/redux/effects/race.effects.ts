import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers/index';
import { RaceActionsCreator, RaceActions } from '../actions/race.actions';

import { AngularFireAuth } from 'angularfire2/auth';
import { RaceService } from '../../services/race.service';

@Injectable()
export class RaceEffects {

    // @Effect() requestMonthHasEvents$ = this._update$
    //     .ofType(RaceActionsCreator.INCREMENT)
    //     .map((action) => this._raceActions.RaceUpdate(action.payload));


    @Effect() requestRaces$ = this._update$
        .ofType(RaceActionsCreator.GET_RACES)
        .map((action) => {
            console.log('###  @Effect() requestRaces$ = this._update$');

            const races = this._raceService.getRaces();

            console.log('NEW RACES FROM FIREBASE', races);

            return this._raceActions.racesLoaded(races);
        });

    @Effect() addRace$ = this._update$
        .ofType(RaceActionsCreator.ADD_RACE)
        .map((action) => {
            console.log('###  @Effect() addRace$ = this._update$');
            console.log(action.payload.race);

            const races = this._raceService.addRace(action.payload.race);

            console.log('addRace Result:', races);

            return true;
        });

    constructor(
        private store: Store<AppState>,
        private _update$: Actions,
        private _raceActions: RaceActions,
        private _raceService: RaceService,
    ) { }
}
