import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers/index';
import { ConfigActionsCreator, ConfigActions } from '../actions/config.actions';
import { FirebaseLoginService } from '../../services/firebaseLogin.service';

import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class ConfigEffects {

    // @Effect() requestMonthHasEvents$ = this._update$
    //     .ofType(ConfigActionsCreator.INCREMENT)
    //     .map((action) => this._configActions.configUpdate(action.payload));


    @Effect() requestLogin$ = this._update$
        .ofType(ConfigActionsCreator.LOGIN)
        .map((action) => {
            console.log('###  @Effect() requestLogin$ = this._update$');
            const user = this._firebaseLoginService.login();

            return this._configActions.loginSuccess(user);
        });

    @Effect() requestLogout$ = this._update$
        .ofType(ConfigActionsCreator.LOGOUT)
        .map((action) => {
            console.log('###  @Effect() requestLogout$ = this._update$');
            const user = this._firebaseLoginService.logout();

            return this._configActions.logoutSuccess();
        });

    constructor (
        private store: Store<AppState>,
        private _update$: Actions,
        private _configActions: ConfigActions,
        private _firebaseLoginService: FirebaseLoginService
    ) {}
}