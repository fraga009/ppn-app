import { EffectsModule } from '@ngrx/effects';
import { ConfigEffects } from './config.effects';
import { RaceEffects } from './race.effects';
import { CategoryEffects } from './category.effects';


export const APP_EFFECTS = [
    EffectsModule.run(ConfigEffects),
    EffectsModule.run(RaceEffects),
    EffectsModule.run(CategoryEffects)
];
