import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers/index';
import { CategoryActionsCreator, CategoryActions } from '../actions/category.actions';

import { AngularFireAuth } from 'angularfire2/auth';
import { CategoryService } from '../../services/category.service';

@Injectable()
export class CategoryEffects {

    // @Effect() requestMonthHasEvents$ = this._update$
    //     .ofType(CategoryActionsCreator.INCREMENT)
    //     .map((action) => this._categoryActions.CategoryUpdate(action.payload));


    @Effect() requestCategories$ = this._update$
        .ofType(CategoryActionsCreator.GET_CATEGORIES)
        .map((action) => {
            console.log('###  @Effect() requestCategories$ = this._update$');

            const categories = this._categoryService.getCategories();

            console.log('NEW CATEGET_CATEGORIES FROM FIREBASE', categories);

            return this._categoryActions.categoriesLoaded(categories);
        });

    @Effect() addCategory$ = this._update$
        .ofType(CategoryActionsCreator.ADD_CATEGORY)
        .map((action) => {
            console.log('###  @Effect() addCategory$ = this._update$');
            console.log(action.payload.category);

            const categories = this._categoryService.addCategory(action.payload.category);

            console.log('addCategory Result:', categories);

            return true;
        });

    constructor(
        private store: Store<AppState>,
        private _update$: Actions,
        private _categoryActions: CategoryActions,
        private _categoryService: CategoryService,
    ) { }
}
