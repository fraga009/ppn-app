import { ConfigActions } from './config.actions';
import { RaceActions } from './race.actions';
import { CategoryActions } from './category.actions';

export const APP_ACTIONS = [
    ConfigActions,
    RaceActions,
    CategoryActions
];
