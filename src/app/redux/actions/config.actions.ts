import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

export class ConfigActionsCreator {
    static INCREMENT = 'INCREMENT'; // @TODO: demo porpose - remove ME
    static LOGIN = 'LOGIN';
    static LOGIN_SUCCESS = 'LOGIN_SUCCESS';
    static LOGIN_FAILED = 'LOGIN_FAILED';
    static LOGOUT = 'LOGOUT';
    static LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
}

@Injectable()
export class ConfigActions {

    // @TODO: demo porpose - remove ME
    public configUpdate(config): Action {
        return {
            type: ConfigActionsCreator.INCREMENT,
            payload: config
        };
    }

    public login(): Action {
        console.log('@@@@ login(): Action');
        return {
            type: ConfigActionsCreator.LOGIN
        };
    }

    public loginSuccess(user): Action {
        console.log('@@@@ loginSuccess(user): Action');
        return {
            type: ConfigActionsCreator.LOGIN_SUCCESS,
            payload: user
        };
    }

    public logout(): Action {
        console.log('@@@@ logout(): Action');
        return {
            type: ConfigActionsCreator.LOGOUT
        };
    }

    public logoutSuccess(): Action {
        console.log('@@@@ logoutSuccess(user): Action');
        return {
            type: ConfigActionsCreator.LOGOUT_SUCCESS,
        };
    }
}