import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { ICategory } from '../../interfaces/category.interface';

export class CategoryActionsCreator {
    static GET_CATEGORIES = 'Get categories data';
    static CATEGORY_LOADED = 'Category data loaded';
    static ADD_CATEGORY = 'Category data loaded';
}

@Injectable()
export class CategoryActions {

    public getCategories(): Action {
        console.log('@@@@ init(): Action');
        return {
            type: CategoryActionsCreator.GET_CATEGORIES
        };
    }

    public addCategory(category: ICategory): Action {
        console.log('@@@@ addCategory(): Action');
        console.log(category);
        return {
            type: CategoryActionsCreator.ADD_CATEGORY,
            payload: {
                category: category
            }
        };
    }

    public categoriesLoaded(categories): Action {
        console.log('@@@@ categoryLoaded(user): Action', categories);
        return {
            type: CategoryActionsCreator.CATEGORY_LOADED,
            payload: {
                categories: categories
            }
        };
    }
}