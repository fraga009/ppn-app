import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { IRace } from '../../interfaces/race.interface';

export class RaceActionsCreator {
    static GET_RACES = 'Get races data';
    static RACE_LOADED = 'Race data loaded';
    static ADD_RACE = 'Race data loaded';
}

@Injectable()
export class RaceActions {

    public getRaces(): Action {
        console.log('@@@@ init(): Action');
        return {
            type: RaceActionsCreator.GET_RACES
        };
    }

    public addRace(race: IRace): Action {
        console.log('@@@@ addRace(): Action');
        console.log(race);
        return {
            type: RaceActionsCreator.ADD_RACE,
            payload: {
                race: race
            }
        };
    }

    public racesLoaded(races): Action {
        console.log('@@@@ raceLoaded(user): Action', races);
        return {
            type: RaceActionsCreator.RACE_LOADED,
            payload: {
                races: races
            }
        };
    }
}