import { ISex, IRole } from './configs.interface';
import { ICategory } from './category.interface';

export interface IMember {
    $key?: number; // firebase id
    name: string;
    surname?: string;
    preferredCategory: ICategory;
    sex: ISex;
    birthDate: Date;
    role: IRole;
}