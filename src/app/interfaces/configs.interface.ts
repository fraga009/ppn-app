export enum ISex {
    female,
    male,
    all
}

export interface IRole {
    $key?: number; // firebase id
    name: string;
    power: number;
    manage: number;
}