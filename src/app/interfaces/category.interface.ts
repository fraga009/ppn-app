import { ISex } from './configs.interface';

export interface ICategory {
    $key?: number; // firebase id
    name: string;
    sex: ISex; // (male,female,all)
    ageStart?: Date;
    ageEnd?: Date;
    noAgeLimits?: boolean;
}