import { ICategory } from './category.interface';

export interface IRaceType {
    $key?: number; // firebase id
    name: string;
    categories: ICategory[];
}

export interface IRace {
    $key?: number; // firebase id
    title: string;
    description: string;
    raceType: IRaceType;
    categories?: ICategory[];
    date: Date;
    deadline: Date;
    avaibleFrom?: Date;
}