// CORE
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

// Firebase
import { AngularFireModule } from 'angularfire2';
// New imports to update based on AngularFire2 version 4
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

// Ngrx (redux)
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { DBModule } from '@ngrx/db';
import { RouterStoreModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

// PPN REDUX
import { APP_REDUCERS } from './redux/index';
import { APP_EFFECTS } from './redux/effects/index';
import { APP_ACTIONS } from './redux/actions/index';

// Others
import { MaterialModule } from '@angular/material';
import { FlashMessagesModule } from 'angular2-flash-messages';

// PPN APP
import { FirebaseService } from './services/firebase.service';
import { RaceService } from './services/race.service';
import { CategoryService } from './services/category.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { superheroesComponent } from './components/superheroes/superheroes.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { superheroComponent } from './components/superhero/superhero.component';
import { AddsuperheroComponent } from './components/add-superhero/add-superhero.component';
import { EditsuperheroComponent } from './components/edit-superhero/edit-superhero.component';
import { AdminPageComponent } from './components/admin/admin-page/admin-page.component';
import { ListUsersComponent } from './components/admin/list-users/list-users.component';
import { ListRacesComponent } from './components/admin/list-races/list-races.component';
import { FirebaseLoginService } from './services/firebaseLogin.service';
import { AddRaceComponent } from './components/admin/add-race/add-race.component';
import { AddCategoryComponent } from './components/admin/add-category/add-category.component';

export const firebaseConfig = {
    apiKey: 'AIzaSyCznOzjzsBk8YZkq-gA80xxr7zBIhfLBrQ',
    authDomain: 'punto-nord.firebaseapp.com',
    databaseURL: 'https://punto-nord.firebaseio.com',
    projectId: 'punto-nord',
    storageBucket: 'punto-nord.appspot.com',
    messagingSenderId: '49938698845'
};

const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'admin', component: AdminPageComponent },
    { path: 'races', component: ListRacesComponent },
    { path: 'categories', component: AddCategoryComponent },
    // {path: 'iscrizione-gare', component: AAAA},

    { path: 'superheroes', component: superheroesComponent },
    { path: 'superhero/:id', component: superheroComponent },
    { path: 'add-superhero', component: AddsuperheroComponent },
    { path: 'edit-superhero/:id', component: EditsuperheroComponent }
];

@NgModule({

    declarations: [
        AppComponent,
        HomeComponent,
        superheroesComponent,
        NavbarComponent,
        superheroComponent,
        AddsuperheroComponent,
        EditsuperheroComponent,
        AdminPageComponent,
        ListUsersComponent,
        ListRacesComponent,
        AddRaceComponent,
        AddCategoryComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(appRoutes),

        FlashMessagesModule,
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFireDatabaseModule,
        AngularFireAuthModule,

        StoreModule.provideStore(APP_REDUCERS),
        StoreDevtoolsModule.instrumentOnlyWithExtension(),
        ...APP_EFFECTS
    ],
    providers: [
        FirebaseService,
        FirebaseLoginService,
        RaceService,
        CategoryService,
        ...APP_ACTIONS
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
// PPN APP
/**
  * TEMPLATES
  *
  *  Pag iscrizioni
  *    - ngFor gare -> Lista gare
  *      - componente iscrizione
  *        - dropdpwn utenti -> Lista utenti
  *        - dropdown categorie -> lista categorie
  *        - campo change si-card
  *        - campo note
  *
  *      - Lista utenti iscritti da me
  *        - rimuovi iscrizione
  *        - cambia dettagli (componente iscrizione)
  *
  *  Pag (o tab) iscritti
  *    - ngFor gare
  *      - lista iscritti
  *      - (if iscritto da me) -> edit (componente iscrizione)
  *
  *  Pag Admin
  *    - ADD gara
  *    - ADD concorrente
  *    - ADD moderatori
  */
/**
  * DATA STRUCTURE
  *
  *  Config
  *    registerAvaibleFrom?: number
  *
  *  Races
  *    $key?: number // firebase id
  *    title: string
  *    description: string
  *    raceType: RaceType
  *    categories?: Categories[]
  *    date: Date
  *    deadline: Date
  *    avaibleFrom?: Date
  *
  *  Race types
  *    $key?: number // firebase id
  *    name: string
  *    categories: Categories[]
  *
  *  Categories
  *    $key?: number // firebase id
  *    name: string
  *    sex: enum (male,female,all)
  *    ageStart: Date
  *    ageEnd: Date
  *
  *  Members
  *    $key?: number // firebase id
  *    name: string
  *    surname?: string
  *    preferredCategory: Category
  *    sex: enum (male,female)
  *    birthDate: Date
  *    role: string
  *
  *  Roles
  *    $key?: number // firebase id
  *    name: string
  *    power: number
  *    manage: number
  */
